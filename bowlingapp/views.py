from django.shortcuts import render
from django.http import JsonResponse

# Create your views here.


def index(request):
    return render(request, 'index.html')

def example(request):
    return render(request, 'history_list.html')

def history(request):
    bowling = {
        'name': 'Johnson',
    }
    return JsonResponse(bowling)