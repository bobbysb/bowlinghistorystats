# https://grn4aewhhg.execute-api.us-east-1.amazonaws.com/prod/games/1113
testroll = "9/4/XX------------"

def parser(roll_string):
    count = 0
    scoreArray = [0] * len(roll_string)
    for i in roll_string:
        if( i == "-" ):
            scoreArray[count] = 0
        elif( i == "/" ):
            scoreArray[count] = 10 - scoreArray[count -1]
        elif( i == "X"):
            scoreArray[count] = 10
        else:
            scoreArray[count] = int(i)
        count = count + 1

    return scoreArray

def frameGroup(scoreArray):
    frameGroupedScores = []
    frame = []
    count = 0
    for item in scoreArray:
        frame.append(item)
        if count % 2 == 1:
            frameGroupedScores.append(frame)
            frame = []

            
        count = count + 1
    return frameGroupedScores


def test_score_parse():
    assert parser(testroll) == [9, 1, 4, 6, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


def test_parse_1111():
    assert parser("11111111111111111111") == [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]

def test_parse_1113():
    assert parser("8-8-8-8-8-8-8-8-8-8-") == [8,0,8,0,8,0,8,0,8,0,8,0,8,0,8,0,8,0,8,0]


def scoring(rollsArray, arrayTotal = False):
    index = 0
    frameCounter = 1
    frameSum = 0
    total = 0
    totalArray = [0] * 10
    rollCounter = 1
    for roll in rollsArray:
        if( frameCounter == 10 ): 
            if ( (roll + rollsArray[index+1]) < 10 ):
                frameSum += roll + rollsArray[index+1]
            else:
                frameSum += roll + rollsArray[index+1] + rollsArray[index+2]
            total += frameSum
            totalArray[frameCounter - 1] = total
            break
        if( rollCounter == 1  ):
            if( roll == 10 ):
                frameSum += roll + rollsArray[index+1] + rollsArray[index+2]
                total += frameSum
                totalArray[frameCounter - 1] = total
                frameSum = 0
                frameCounter += 1
            else:
                frameSum += roll 
                rollCounter += 1
            
        elif( rollCounter == 2 ):
            frameSum += roll 
            if(frameSum == 10 ):
                total += frameSum + rollsArray[index+1]
                totalArray[frameCounter - 1] = total
                frameCounter += 1
                frameSum = 0
                rollCounter = 1
            else:
                rollCounter = 1
                total += frameSum
                totalArray[frameCounter - 1] = total
                frameCounter += 1
                frameSum = 0

        index += 1
    
    if(arrayTotal == False):
        return total
    else:
        return totalArray

def test_scoring_all_gutter_balls():
    assert scoring([0] * 10) == 0

def test_scoring_two():
    assert scoring([1] * 2) == 2

def test_scoring_spare():
    assert scoring( [5, 5, 1, 0] ) == 12

def test_scoring_strike():
    assert scoring( [10, 10, 7, 2, 0, 0] ) == 55

def test_scoring_bonus():
    assert scoring( [0, 0, 0, 0, 9, 1, 1]) == 11

def test_scoring_1116():
    assert scoring(
        [   0, 0, 0, 0, 
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            10, 1, 1]
        ) == 14

def test_scoring_1117():
    assert scoring(
        [   0, 0, 0, 0, 
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 1, 9, 1, 1]
        ) == 12

def test_scoring_bowlingscore_com():
    assert scoring(
        [  5, 5, 1, 0, 7, 2, 4, 4, 4, 1, 10, 7, 3, 1, 6, 3, 0, 0, 10, 7]
        ) == 92

def test_scoring_bowlingscore_com2():
    assert scoring(
        [  5, 5, 1, 0, 7, 2, 4, 4, 4, 1, 10, 7, 3, 1, 6, 3, 0, 0, 10, 7], True
        ) == [11,12,21,29,34, 54, 65, 72, 75, 92]

def test_scoring_bowlingscore_com_three_ending_strikes():
    assert scoring(
        [  6, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 10], True
        ) == [8,12,12,12,12, 12, 12, 12, 12, 42]