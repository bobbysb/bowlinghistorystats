

class GreetingBuilder():
    def __init__(self):
        pass
    def build(self, name = "World"):
        return "Hello, " + name + "!"

import pytest

def test_GreetingBuilder():
    greeting = GreetingBuilder()
    buildGreeting = greeting.build()
    assert buildGreeting == "Hello, World!"


def test_GreetingBuilder_name():
    greeting = GreetingBuilder()
    name = "George"
    buildGreeting = greeting.build(name)
    assert buildGreeting == "Hello, George!"




