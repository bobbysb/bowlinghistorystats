
from django.conf.urls import url, include
from bowlingapp.views import index, history, example

app_name = 'bowlingapp'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^history/$', history, name='history'),
    url(r'^example/$', example, name='example'),
]
