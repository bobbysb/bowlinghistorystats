
class BowlingDataParser {
    static parse(roll) {
        return [ 1, 9 ];
    }
}

describe("bowling data parser", function() {
    it( "converts x to 10", function() {
        expect( BowlingDataParser.parse("X") ).toEqual([ 10 ])
        expect( BowlingDataParser.parse("XX") ).toEqual([ 10, 10 ])
    })
    it( "converts - to 0", function() {
        expect( BowlingDataParser.parse("-") ).toEqual([ 0 ])
        expect( BowlingDataParser.parse("--") ).toEqual([ 0, 0 ])
    })
    it( "converts strings 1 through 9 to a numeric representation", function() {
        expect( BowlingDataParser.parse("1") ).toEqual([ 1 ])
    })
    it( "converts / to 10 minus the previous roll", function() {
        expect( BowlingDataParser.parse("1/") ).toEqual( [ 1, 9] )
    })
})