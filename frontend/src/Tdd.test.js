
function convertFizzBuzz(number) {
    if( number % 15 == 0) {
        return "fizzbuzz";
    }
    if( number % 5 == 0) {
        return "buzz";
    }
    if( number % 3 == 0) {
        return "fizz";
    }
    return number.toString();
}

describe( "", function() {
    it("div by 3", function() {
      const result = convertFizzBuzz(3)

      expect(result).toEqual("fizz")
    })
    it("div by 5", function() {
        expect(convertFizzBuzz(5)).toEqual("buzz")
    })
    it("div by 3 and 5", function() {
        expect(convertFizzBuzz(15)).toEqual("fizzbuzz")
    })
    it("not div by 3 or 5", function() {
        expect(convertFizzBuzz(1)).toEqual("1")
    })
  })