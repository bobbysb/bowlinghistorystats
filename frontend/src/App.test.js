import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// it('renders without crashing', () => {
//   const div = document.createElement('div');
//   ReactDOM.render(<App />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });

class GreetingBuilder {
  build ( name = "World" ) {
    return "Hello, " + name + "!"
  }
}


describe( "Greeting builder" , function() {
  it( "returns a default message", function () {
    const sut = new GreetingBuilder();

    const result = sut.build();

    expect(result).toEqual( "Hello, World!");
  })
  it( "accepts name argument", function() {
    const sut = new GreetingBuilder();

    const result = sut.build("George");

    expect(result).toEqual( "Hello, George!");
  })
});


